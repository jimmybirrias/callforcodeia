from sklearn.externals import joblib
from random import randint
import os.path
import pandas as pd
import json

def main():
    # data = read_test_elements('~/PycharmProjects/CallforcodePrediction/data/test_element.csv', "ISO-8859-1", ['Poblaci_n femenina', 'No.', 'Municipio', 'Colonia'])
    # rows, cols = data.shape
    # clf = load_model('./TrainedModels/LinearRegression.pk1')
    # n = randint(0, (rows - 1))
    # result = clf.predict([data.loc[n]])
    # print('Result ==>', result)
    json_read('../data/products_weight.json')


def read_test_elements(file, encoding="ISO-8859-1", drop_elements=[]):
    data = pd.read_csv(file, encoding=encoding)
    data = data.drop(drop_elements, axis=1)
    return data


def load_model(file):
    print('Loading the following model ===> ', file)
    if os.path.isfile(file):
        return joblib.load(file)
    else:
        print('The model file does not exits')
        exit(1)


def json_read(file):
    tmp_list = {}
    with open(file, 'r') as f:
        data = json.load(f)

    for product in data['product_list']:
        if product['type']:
            tmp_list[product['type']] = product['products']

    list = sorted(tmp_list.items(), key=lambda x: x[1], reverse=False)
    print(list)


if __name__ == '__main__':
    print('main')
    # main()
