from sklearn.linear_model import LinearRegression
from sklearn.cross_validation import train_test_split
from sklearn import ensemble
from sklearn.externals import joblib

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def main_train():
    data = pd.read_csv('~/PycharmProjects/CallforcodePrediction/data/colsociodemo2010amg.csv', encoding="ISO-8859-1")
    reg = LinearRegression()

    labels = data['Poblaci_n femenina']
    train1 = data.drop(['Poblaci_n femenina', 'No.', 'Municipio', 'Colonia'], axis=1)

    x_train, x_test, y_train, y_test = train_test_split(train1, labels, test_size=0.10, random_state=2)
    reg.fit(x_train, y_train)
    reg_score = reg.score(x_test, y_test)
    print('REG prediction ====> {0:.2f} %'.format(100 * reg_score))

    clf = ensemble.GradientBoostingRegressor(n_estimators=400, max_depth=5, min_samples_split=2, learning_rate=0.1,
                                             loss='ls')
    clf.fit(x_train, y_train)
    clf_score = clf.score(x_test, y_test)
    print('CFG prediction ====>  {0:.2f} %'.format(100 * clf_score))

    if reg_score > clf_score:
        print('REG is grather')
    else:
        print('CLF is grather')

    t_sc = np.zeros(400, dtype=np.float64)
    y_pred = reg.predict(x_test)

    for i, y_pred in enumerate(clf.staged_predict(x_test)):
        t_sc[i] = clf.loss_(y_test, y_pred)

    testsc = np.arange(400) + 1

    plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plt.plot(testsc, clf.train_score_, 'b-', label='Set dev train')
    plt.plot(testsc, t_sc, 'r-', label='set dev test')
    plt.show()

    joblib.dump(clf, 'GradientBoostingRegressor.pk1')
    clf = joblib.load('GradientBoostingRegressor.pk1')

    result = clf.score(x_test, y_test)
    print("Test score: {0:.2f} %".format(100 * result))


main_train()
