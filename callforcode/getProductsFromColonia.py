#!/usr/bin/python3.5

import mysql.connector
import json
import sys

table = 'colonia'


def get_connect(host, port, database, user, password):
    hostname = host
    return mysql.connector.connect(host=hostname, port=port, database=database, user=user, password=password, auth_plugin='mysql_native_password')


def get_cursor(db_connection):
    return db_connection.cursor()


def execute_query(query, db_cursor):
    db_cursor.execute(query)
    return db_cursor.fetchall()


def product_list_read(file):
    tmp_list = {}
    with open(file, 'r') as f:
        data = json.load(f)

    for product in data['product_list']:
        if product['type']:
            tmp_list[product['type']] = product['products']

    return tmp_list.items()


if __name__ == '__main__':
    cnx = get_connect('sl-us-south-1-portal.34.dblayer.com', 56940, 'ayudapp', 'admin', 'DAWDZIQLFNINPKGG')
    cursor = get_cursor(cnx)
    query = 'SELECT poblacion_total, poblacion_masculina, poblacion_femenina FROM {0} WHERE nombre = "{1}" LIMIT 1'.format(table, sys.argv[1])
    res = execute_query(query, cursor)
    product_list = product_list_read('../data/products_weight.json')

    final = {}

    for tmp in product_list:
        if tmp[0] == 'femenino':
            inventario = {}
            for product in tmp[1]:
                inventario[product['name']] = product['required_per_person'] * res[0][2]
            final['femenino'] = inventario

        if tmp[0] == 'masculino':
            inventario = {}
            for product in tmp[1]:
                inventario[product['name']] = product['required_per_person'] * res[0][1]
            final['masculino'] = inventario

        if tmp[0] == 'general':
            inventario = {}
            for product in tmp[1]:
                inventario[product['name']] = product['required_per_person'] * res[0][0]
            final['general'] = inventario
    print(str(final).replace('\'', '"'))






