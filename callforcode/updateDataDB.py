#!/usr/bin/python3.5

import pandas as pd
import mysql.connector
import sys

# mysql -u admin -pDAWDZIQLFNINPKGG --host sl-us-south-1-portal.34.dblayer.com --port 56940 --ssl-mode=REQUIRED
cnx = mysql.connector.connect(host='sl-us-south-1-portal.34.dblayer.com', port=56940, database='ayudapp', user='admin', password='DAWDZIQLFNINPKGG')

cursor = cnx.cursor()
data = pd.read_csv(sys.argv[1], encoding=sys.argv[2])
data = data.drop(['No.','Relaci_n hombres-mujeres','Promedio de hijos nacidos vivos','Poblaci_n nacida en la entidad','Poblaci_n masculina nacida en la entidad','Poblaci_n femenina nacida en la entidad','Poblaci_n nacida en otra entidad','Poblaci_n masculina nacida en otra entidad','Poblaci_n femenina nacida en otra entidad',
                  'Poblaci_n de 5 a_os y m_s residente en la entidad en junio de 2005','Poblaci_n masculina de 5 a_os y m_s residente en la entidad en junio de 2005','Poblaci_n femenina de 5 a_os y m_s residente en la entidad en junio de 2005','Poblaci_n de 5 a_os y m_s residente en otra entidad en junio de 2005',
                  'Poblaci_n masculina de 5 a_os y m_s residente en otra entidad en junio de 2005','Poblaci_n femenina de 5 a_os y m_s residente en otra entidad en junio de 2005','Poblaci_n de 3 a_os y m_s que habla alguna lengua ind_gena','Poblaci_n masculina de 3 a_os y m_s que habla alguna lengua ind_gena',
                  'Poblaci_n femenina de 3 a_os y m_s que habla alguna lengua ind_gena','Poblaci_n de 3 a_os y m_s que habla alguna lengua ind_gena y no habla espa_ol','Poblaci_n masculina de 3 a_os y m_s que habla alguna lengua ind_gena y no habla espa_ol','Poblaci_n femenina de 3 a_os y m_s que habla alguna lengua ind_gena y no habla espa_ol',
                  'Poblaci_n de 3 a_os y m_s que habla alguna lengua ind_gena y habla espa_ol','Poblaci_n masculina de 3 a_os y m_s que habla alguna lengua ind_gena y habla espa_ol','Poblaci_n femenina de 3 a_os y m_s que habla alguna lengua ind_gena y habla espa_ol','Poblaci_n de 5 a_os y m_s que habla alguna lengua ind_gena',
                  'Poblaci_n de 5 a_os y m_s que habla alguna lengua ind_gena y no habla espa_ol','Poblaci_n de 5 a_os y m_s que habla alguna lengua ind_gena y habla espa_ol','Poblaci_n en hogares censales ind_genas',
                  "Poblaci_n con limitaci_n en la actividad","Poblaci_n con limitaci_n para caminar o moverse, subir o bajar","Poblaci_n con limitaci_n para ver, a_n usando lentes","Poblaci_n con limitaci_n para hablar, comunicarse o conversar",
                  'Poblaci_n con limitaci_n para escuchar',"Poblaci_n con limitaci_n para vestirse, ba_arse o comer",'Poblaci_n con limitaci_n para poner atenci_n o aprender cosas sencillas','Poblaci_n con limitaci_n mental',
                  'Poblaci_n sin limitaci_n en la actividad','Poblaci_n de 3 a 5 a_os que no asiste a la escuela','Poblaci_n masculina de 3 a 5 a_os que no asiste a la escuela','Poblaci_n femenina de 3 a 5 a_os que no asiste a la escuela',
                  'Poblaci_n de 6 a 11 a_os que no asiste a la escuela','Poblaci_n masculina de 6 a 11 a_os que no asiste a la escuela','Poblaci_n femenina de 6 a 11 a_os que no asiste a la escuela','Poblaci_n de 12 a 14 a_os que no asiste a la escuela',
                  'Poblaci_n masculina de 12 a 14 a_os que no asiste a la escuela','Poblaci_n femenina de 12 a 14 a_os que no asiste a la escuela','Poblaci_n de 15 a 17 a_os que asiste a la escuela','Poblaci_n masculina de 15 a 17 a_os que asiste a la escuela',
                  'Poblaci_n femenina de 15 a 17 a_os que asiste a la escuela','Poblaci_n de 18 a 24 a_os que asiste a la escuela','Poblaci_n masculina de 18 a 24 a_os que asiste a la escuela','Poblaci_n femenina de 18 a 24 a_os que asiste a la escuela',
                  'Poblaci_n de 8 a 14 a_os que no saben leer y escribir','Poblaci_n masculina de 8 a 14 a_os que no saben leer y escribir','Poblaci_n femenina de 8 a 14 a_os que no saben leer y escribir','Poblaci_n de 15 a_os y m_s analfabeta',
                  'Poblaci_n masculina de 15 a_os y m_s analfabeta','Poblaci_n femenina de 15 a_os y m_s analfabeta','Poblaci_n de 15 a_os y m_s sin escolaridad','Poblaci_n masculina de 15 a_os y m_s sin escolaridad','Poblaci_n femenina de 15 a_os y m_s sin escolaridad',
                  'Poblaci_n de 15 a_os y m_s con primaria incompleta','Poblaci_n masculina de 15 a_os y m_s con primaria incompleta','Poblaci_n femenina de 15 a_os y m_s con primaria incompleta','Poblaci_n de 15 a_os y m_s con primaria completa','Poblaci_n masculina de 15 a_os y m_s con primaria completa',
                  'Poblaci_n femenina de 15 a_os y m_s con primaria completa','Poblaci_n de 15 a_os y m_s con secundaria incompleta','Poblaci_n masculina de 15 a_os y m_s con secundaria incompleta','Poblaci_n femenina de 15 a_os y m_s con secundaria incompleta',
                  'Poblaci_n de 15 a_os y m_s con secundaria completa','Poblaci_n masculina de 15 a_os y m_s con secundaria completa','Poblaci_n femenina de 15 a_os y m_s con secundaria completa','Poblaci_n de 18 a_os y m_s con educaci_n pos-b_sica','Poblaci_n masculina de 18 a_os y m_s con educaci_n pos-b_sica',
                  'Poblaci_n femenina de 18 a_os y m_s con educaci_n pos-b_sica','Grado promedio de escolaridad','Grado promedio de escolaridad de la poblaci_n masculina','Grado promedio de escolaridad de la poblaci_n femenina','Poblaci_n econ_micamente activa','Poblaci_n masculina econ_micamente activa',
                  'Poblaci_n femenina econ_micamente activa','Poblaci_n no econ_micamente activa','Poblaci_n masculina no econ_micamente activa','Poblaci_n femenina no econ_micamente activa','Poblaci_n ocupada','Poblaci_n masculina ocupada','Poblaci_n femenina ocupada','Poblaci_n desocupada','Poblaci_n masculina desocupada',
                  'Poblaci_n femenina desocupada','Poblaci_n sin derechohabiencia a servicios de salud','Poblaci_n derechohabiente a servicios de salud','Poblaci_n derechohabiente del IMSS','Poblaci_n derechohabiente del ISSSTE','Poblaci_n derechohabiente del ISSSTE estatal','Poblaci_n derechohabiente del seguro popular o Seguro M_dico para una Nueva Generaci_n',
                  'Poblaci_n soltera o nunca unida de 12 a_os y m_s','Poblaci_n casada o unida de 12 a_os y m_s','Poblaci_n que estuvo casada o unida de 12 a_os y m_s','Poblaci_n con religi_n cat_lica',"Protestantes, Evang_licas y B_blicas diferentes de evang_licas",'Poblaci_n con otras religiones diferentes a las anteriores',
                  'Poblaci_n sin religi_n','Total de hogares censales','Hogares censales con jefatura masculina','Hogares censales con jefatura femenina','Poblaci_n en hogares censales','Poblaci_n en hogares censales con jefatura masculina','Poblaci_n en hogares censales con jefatura femenina','Ocupantes en viviendas particulares habitadas',
                  'Promedio de ocupantes en viviendas particulares habitadas','Promedio de ocupantes por cuarto en viviendas particulares habitadas','Total de Viviendas','Viviendas particulares habitadas con piso de material diferente de tierra','Viviendas particulares habitadas con piso de tierra','Viviendas particulares habitadas con un dormitorio',
                  'Viviendas particulares habitadas con dos dormitorios y m_s','Viviendas particulares habitadas con un solo cuarto','Viviendas particulares habitadas con dos cuartos','Viviendas particulares habitadas con 3 cuartos y m_s','Viviendas particulares habitadas que disponen de luz el_ctrica','Viviendas particulares habitadas que no disponen de luz el_ctrica',
                  'Viviendas particulares habitadas que disponen de agua entubada en el _mbito de la vivienda','Viviendas particulares habitadas que no disponen de agua entubada en el _mbito de la vivienda','Viviendas particulares habitadas que disponen de excusado o sanitario','Viviendas particulares habitadas que disponen de drenaje','Viviendas particulares habitadas que no disponen de drenaje',
                  "Viviendas particulares habitadas que disponen de luz el_ctrica, agua entubada de la red p_blica y drenaje",'Viviendas particulares habitadas sin ning_n bien','Viviendas particulares habitadas que disponen de radio','Viviendas particulares habitadas que disponen de televisor','Viviendas particulares habitadas que disponen de refrigerador','Viviendas particulares habitadas que disponen de lavadora',
                  'Viviendas particulares habitadas que disponen de autom_vil o camioneta','Viviendas particulares habitadas que disponen de computadora','Viviendas particulares habitadas que disponen de l_nea telef_nica fija','Viviendas particulares habitadas que disponen de tel_fono celular','Viviendas particulares habitadas que disponen de internet'], axis=1)
row, col = data.shape

print('Row ===>', row, 'Col ===>', col)

for n in range(row):
    current = data.loc[n]
    sql = "INSERT INTO colonia (municipio, nombre, poblacion_total, poblacion_masculina, poblacion_femenina, " \
          "poblacion_0_2, poblacion_masculina_0_2, poblacion_femenina_0_2, poblacion_3_mas, poblacion_masculina_3_mas, poblacion_femenina_3_mas, " \
          "poblacion_5_mas, poblacion_masculina_5_mas, poblacion_femenina_5_mas, poblacion_12_mas, poblacion_masculina_12_mas, poblacion_femenina_12_mas, " \
          "poblacion_15_mas, poblacion_masculina_15_mas, poblacion_femenina_15_mas, poblacion_18_mas, poblacion_masculina_18_mas, poblacion_femenina_18_mas, " \
          "poblacion_3_5, poblacion_masculina_3_5, poblacion_femenina_3_5, poblacion_6_11, poblacion_masculina_6_11, poblacion_femenina_6_11, " \
          "poblacion_8_14, poblacion_masculina_8_14, poblacion_femenina_8_14, poblacion_12_14 , poblacion_masculina_12_14 , poblacion_femenina_12_14 , poblacion_15_17 , poblacion_masculina_15_17 , poblacion_femenina_15_17, " \
          "poblacion_18_24, poblacion_masculina_18_24, poblacion_femenina_18_24, poblacion_femenina_15_49, poblacion_60_mas, poblacion_masculina_60_mas, poblacion_femenina_60_mas, " \
          "poblacion_15_64, poblacion_65_mas) VALUES('{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},{}, {}, {}, {}, {}, {})"

    val = []
    for i in range(0, 48):
        val.append(current[i])
    cursor.execute(sql.format(*val))
    cnx.commit()
    print(cursor.rowcount, 'Colonia inserted')

