/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions about the command line
*/
var { exec } = require('child_process');
var config = require('config');

var pythonPath = config.get('python.path');
var pythonScript = config.get('python.script');

module.exports = {
    getInventario: function(colonia, callback){
        exec(pythonPath + ' ' + pythonScript + ' "' + colonia + '"', function(err, stdout, stderr){
            if(err){
                callback(err, 'Error tring to execute the command ' + stderr);
            } else {
                callback('', stdout);
            }
        });
    }
}