/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions to connect zip service
*/
var req = require('request');
var config = require('config')

module.exports = {
    getColonias: function(zip){
        console.log('in zip.getColonias ====>', zip);
        var options = {
            method: 'GET',
            rejectUnauthorized: false,
            url: config.get('zip.apiurl') + zip,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }

        return new Promise(function(resolve, reject){
            req.get(options, function(err, res, body){
                if(err){
                   reject(err);
                } else {
                    var final = [];
                    var tmp = JSON.parse(body);
                    var municipio = tmp.municipio;
                    for (let index = 0; index < tmp.colonias.length; index++) {
                        var x = {
                            nombre: tmp.colonias[index],
                            municipio: municipio
                        }; 
                        final.push(x);
                    }
                    resolve(final);
                }
            });
        });
    }
}