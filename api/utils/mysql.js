/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions to connect with mysql
*/
var mysql = require('mysql');
var config = require('config');

module.exports = {
    connect: function(){
        var cnx = mysql.createConnection({
            host: config.get('mysql.host'),
            port: config.get('mysql.port'),
            user: config.get('mysql.user'),
            password: config.get('mysql.password'),
            database: config.get('mysql.database')
        });
        cnx.connect();
        return cnx;
    }, 
    queryColonia: function(nombre, municipio, cnx){
        var query = 'SELECT poblacion_total FROM colonia WHERE nombre = "' + nombre +'" AND municipio = "' + municipio + '"';
        console.log('Query ====>', query);

        return new Promise(function(resolve, reject){
            cnx.query(query, function(error, results, fields) {
                if(error) {
                    reject(error);
                } else {
                    resolve(results);
                }
            });
        });
    }, 
    getColonias: function(code, cnx){
        var query = 'SELECT cp, municipio, estado, nombre FROM colonia WHERE cp = ' + code + ''; 
        console.log('Query ===>', query);

        return new Promise(function(resolve, reject){
            cnx.query(query, function(error, results, fields){
                if(error) {
                    reject(error);
                } else {
                    resolve(results);
                }  
            });
        });
    }
}