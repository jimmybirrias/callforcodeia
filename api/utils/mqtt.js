/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions to connect to mqtt services
*/
var mqtt = require('mqtt');
var config = require('config');

var broker = config.get('mqtt.broker');
var channel = config.get('mqtt.channel');

module.exports = {
    getClient : function(){
        return mqtt.connect('mqtt://' + broker)
    }, 
    publish : function(client, topic, message){
        client.publish(topic, String(message));
    }
};