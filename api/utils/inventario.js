/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions to read product catalog
*/
var fs = require('fs');

module.exports = {
    read: function(path, callback){
        fs.readFile(path, function(err, data){
            if(err){
                callback(err, 'Something wrong trying to open the ' + path);
            } else {
                callback('', JSON.parse(data));
            }
        });
    }
}