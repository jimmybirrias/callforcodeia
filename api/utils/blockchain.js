/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: util to define common functions to connect with the blockchain service 
*/
var config = require('config');
var request = require('request');
var qs = require('querystring');

module.exports = {
    getEvent: function(id){
        var options = {
            url: config.get('blockchain.url') + config.get('blockchain.eventpath') + id,
            method: 'GET',
            json: true
        };

        return new Promise(function (resolve, reject){
            request(options, function(err, res, body){
                if(err){
                    console.error(err);
                    reject(err);
                } else {
                    if(body.error){
                        console.log('blockchain.getEvent ====>', body);
                        reject(body);
                    } else {
                        resolve(body);
                    }
                }
            });
        });
    },
    updateEvent: function(event, id, callback){
        var options = {
            url: config.get('blockchain.url') + config.get('blockchain.eventpath') + id,
            method: 'PUT',
            json: true,
            body: event
        };
        request(options, function(err, res, body){
            if(err){
                callback(err, 'Something wrong updating ====> ' + id);
            } else {
                if(body.error){
                    callback(body.error, 'Something wrong updating ====> ' + id);
                } else {
                    callback('', body);
                }
            }
        });
    },
    getDonationPerEvent: function(id, type){
        var filter = {
            where: {
                and: [
                    {type: String(type).toUpperCase()},
                    {eventDonation: 'resource:org.example.ayudapp.EventDonation#' + id}
                ]
            }
        };
        console.log('FILTER ====>',qs.escape(JSON.stringify(filter)));
        var options = {
            url: config.get('blockchain.url') + config.get('blockchain.donationpath') + '?filter=' + qs.escape(JSON.stringify(filter)),
            method: 'GET',
            json: true
        };

        return new Promise(function (resolve, reject){
            request(options, function(err, res, body){
                if(err){
                    console.error(err);
                    reject(err);
                } else {
                    if(body.error){
                        console.log('blockchain.getDonation ====>', body);
                        reject(body);
                    } else {
                        var tmp = body;
                        var donatios = [];
                        for (let index = 0; index < tmp.length; index++) {
                            var obj = {};
                            obj[tmp[index].name] = tmp[index].quantity
                            donatios.push(obj);
                        }
                        resolve(donatios);
                    }
                }
            });
        });
    }
};