/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: REST api endpoint to handle the donations
*/
var express = require('express');
var config = require('config');
var blockchain = require('../utils/blockchain');
var router = express.Router();

var leave_top = config.get('analytics.leave_top_percent');
var trust = config.get('analytics.trust_on_intentions');
var original_list = config.get('analytics.initila_list');
var keys = config.get('analytics.keys_products');

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now(), 'Path', req.originalUrl);
    next();
});

router.get('/', function(req, res){
    res.send(config.get('analytics'));
});

router.post('/', function(req, res){
    var event = req.body;
    var baseline = event.baseline;
    getDonationsPerEvent(event.uuid, function(error, result){
        if(error){
            res.send(error);
        } else {
            if(process.env.NODE_ENV == 'dev')
                console.log('blockchain result', result)
            //Here compare
            var values = compareProducts(baseline, result);
            var tmp = values['new_top_list'];
            var completes = values['completes'];
            
            console.log('Predicted list ====>', tmp, 'Completes products ====>', completes);
            var differents = original_list.filter(function(obj){
                return tmp.indexOf(obj) == -1;
            });

            var differents = differents.filter(function(obj){
                return completes.indexOf(obj) == -1;
            });
            console.log('Clean elements ====>', differents);
            var randomIndexes = generateList(differents.length - 1);
            console.log('Base line index', randomIndexes);
            if(tmp.length < 5){
                var limit = 5 - tmp.length;
                console.log('Number of elements to add ====>', limit);
                for (let index = 0; index < limit ; index++) {
                    console.log('adding element from the default list', differents[randomIndexes[index]], index);
                    tmp.push(differents[randomIndexes[index]].toLowerCase());  
                }

            } else if(tmp.length > 5) {
                var x = [];
                for (let index = 0; index < 5; index++) {
                    x.push(tmp[index]);
                }
                tmp = x;
            }
            res.json(tmp.filter(deleteDuplicates));
        }
    });
});

function generateList(max){
    var arr = []
    while(arr.length < 5){
        var randomnumber = Math.floor(Math.random()*max) + 1;
        if(arr.indexOf(randomnumber) > -1) continue;
        arr[arr.length] = randomnumber;
    }
    return arr;
}

function deleteDuplicates(value, index, self){
    return self.indexOf(value) === index;
}

async function getDonationsPerEvent(id, callback){
    var donations = {};
    try{
        for (let index = 0; index < keys.length; index++) {
            var tmp = await blockchain.getDonationPerEvent(id, keys[index].key);
            var newOne = findAndSum(tmp);
            donations[keys[index].key] = newOne;
        }
        callback('', donations);
    }catch(e){
        callback(e, 'The donations can not be consulted');
    }
}

function compareProducts(baseline, current){
    if(process.env.NODE_ENV == 'dev')
        console.log('donaciones.compareProducts');
    var new_top_list = [];
    var completes = [];
    var tmp = current;
    var tmpOriginal = baseline.products;

    Object.keys(tmp).forEach(element => {
        if(process.env.NODE_ENV == 'dev')
            console.log('Checking category ====>', element);
        //categories
        var categories = tmp[element];

        Object.keys(categories).forEach(index => {
            if(process.env.NODE_ENV == 'dev')
                console.log('Element ====>', index);
            var currentElement = categories[index];
            Object.keys(currentElement).forEach(key => {
                if(process.env.NODE_ENV == 'dev')
                    console.log('Object ====>', key);
                var originalValue = getValueFromJSON(tmpOriginal, element, key);
                if(process.env.NODE_ENV == 'dev')
                    console.log(key, currentElement[key], 'vs', key, originalValue);
                if(currentElement[key] <= originalValue * (leave_top * trust)){
                    new_top_list.push(key.toLowerCase());
                } else {
                    completes.push(key.toLowerCase());
                }
            });
        });
        
    });
    return {new_top_list, completes};
}

function findAndSum(donations){
    var newOne = {};
    for (let index = 0; index < donations.length; index++) {
        var tmp = donations[index];
        if(newOne[getFirstKeyFromJson(donations[index])]){
            newOne[getFirstKeyFromJson(donations[index])] += tmp[getFirstKeyFromJson(donations[index])];
        } else {
            newOne[getFirstKeyFromJson(donations[index])] = tmp[getFirstKeyFromJson(donations[index])];
        }
    }

    var tmpArray = [];
    Object.keys(newOne).forEach(element => {
        var obj = {};
        obj[element] = newOne[element];    
        tmpArray.push(obj);
    });
    if(process.env.NODE_ENV == 'dev')
        console.log('Donations sum', tmpArray);
    return tmpArray;
}

function getFirstKeyFromJson(obj){
    var tmp = Object.keys(obj)[0];
    return tmp;
}

function getValueFromJSON(list, category, key){
    var value = 0;
    Object.keys(list).forEach(element => {
        if(element == category){
            if(process.env.NODE_ENV == 'dev')
                console.log('Looking in', category);
            var tmp = list[category];
            Object.keys(tmp).forEach(index => {
                var x = getFirstKeyFromJson(tmp[index]);
                var y = tmp[index];
                if(process.env.NODE_ENV == 'dev')
                    console.log('Here ====>', x.toLowerCase(), y, key.toLowerCase());
                if(x.toLowerCase() === key.toLowerCase()){
                    value = y[x];
                }
            });
        }
    });
    return value;
}

module.exports = router