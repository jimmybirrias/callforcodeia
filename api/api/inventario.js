/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: REST api endpoint to handle the inventory
*/
var express = require('express');
var mysql = require('../utils/mysql');
var productos = require('../utils/inventario');
var zip = require('../utils/zip');
var path = './data/NewProductList.json';

var router = express.Router();
var key = {
    food : 'food',
    tools : 'tools',
    medicine : 'medicine',
    other: "other"
};

var products_weight = [];
var cnx = mysql.connect();

productos.read(path, function(error, data){
    if(!error){
        var inv = data.product_list;
        for(let i = 0; i < inv.length; i++){
            products_weight.push(inv[i]);
        }
    } else {
        console.error('Something wrong trying to load the JSON property files');
    }
});

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now(), 'Path', req.originalUrl);
    next();
});

router.get('/', function (req, res) {
    productos.read(path, function(error, data){
        if(!error){
            res.json(data);
        } else {
            res.send(error);
        }
    });
}); 

router.post('/', function(req, res){
    var evento = req.body;
    parseColonias(evento.colonias, function(error, result){
        if(error){
            res.statusCode = 400;
            res.send(error);
        } else {
            res.json(result);
        }
    });
});

router.post('/zip', function(req, res){
    var evento = req.body;
    var codes = evento.zips;
    console.log('Zips ====>', codes);

    getColoniasFromZip(codes, function(error, result){
        if(error){
            res.statusCode = 400;
            res.send(error);
        } else {
            parseColonias(result, function(e, r){
                if(e){
                    res.statusCode = 400;
                    res.send(error);
                } else {
                    res.json(r);
                }
            });
        }
    });
});

async function getColoniasFromZip(codes, callback){
    var total = [];
    try{
        for (let index = 0; index < codes.length; index++) {
            var code = codes[index];
            var colonias = await zip.getColonias(code);
            for (let i = 0; i < colonias.length; i++) {
                total.push(colonias[i]);    
            }
        }
        callback('', total);
    }catch(e){
        callback(e, 'Something wrong ')
    }
}

async function parseColonias(colonias, callback){
    var total = 0;
    var current = [];
    try{
        for(let i = 0; i < colonias.length; i++){
            var tmp =  await mysql.queryColonia(colonias[i].nombre, colonias[i].municipio, cnx);
        
            if(tmp.length == 0){
                console.log('No errors returned for the query');
            } else {
                var x = {
                    poblacion_total: tmp[0].poblacion_total,
                };
                var y = generateMainResponse(colonias[i], x);
                total+=y.affected_people;
                delete y.affected_people;
                current.push(y);
            }
        }
        var products = mainCatalog(current);
        var etc = {
            'affected_people': total,
            'products': products
        };
        callback('', etc);
    }catch(error){
        callback(error, 'Something wrong');
    }
}

function generateMainResponse(colonia, inventario){
    console.log('Generating products to', colonia);
    var res = {
        'affected_people': inventario.poblacion_total,
        'product_list': inventario
    };

    if(inventario.length == 0){
        console.log('No results to ', colonia);
        res.product_list = [];
        return res;
    } else {
        var z1 = [];

        for (let index = 0; index < products_weight.length; index++) {
            var tmp = products_weight[index];

            if(tmp.type == key.food){
                var x1 = {};
                var x2 = tmp.products;
                for(let i = 0; i < x2.length; i++){
                    x1[x2[i].name] = x2[i].required_per_person_per_week * res.product_list.poblacion_total;
                }
                z1.push({'food' : x1});
            }

            if(tmp.type == key.tools){
                var x1 = {};
                var x2 = tmp.products;
                for(let i = 0; i < x2.length; i++){
                    x1[x2[i].name] = x2[i].required_per_person_per_week * res.product_list.poblacion_total;
                }
                z1.push({'tools': x1});
            }

            if(tmp.type == key.medicine){
                var x1 = {};
                var x2 = tmp.products;
                for(let i = 0; i < x2.length; i++){
                    x1[x2[i].name] = x2[i].required_per_person_per_week * res.product_list.poblacion_total;
                }
                z1.push({'medicine' : x1});
            }

            if(tmp.type == key.other){
                var x1 = {};
                var x2 = tmp.products;
                for(let i = 0; i < x2.length; i++){
                    x1[x2[i].name] = x2[i].required_per_person_per_week * res.product_list.poblacion_total;
                }
                z1.push({'other': x1});
            }
        }
        res.product_list = z1;
        return res;
    }
}

function mainCatalog(product_list){
    var products = {
        "food" : [],
        "tools" : [],
        "medicine": [],
        "other": []
    }

    for (let index = 0; index < product_list.length; index++) {
        var tmp = product_list[index].product_list;
        for (let i = 0; i < tmp.length; i++) {
            var x = tmp[i];
            Object.keys(x).forEach(element => {
                var currentKey = element;
                console.log('======= ' + currentKey + ' =======');
                var currentObject = x[element];
                var count = 0;
                Object.keys(currentObject).forEach(element2 => {
                    console.log(element2, currentObject[element2]);
                    var currentCat = products[currentKey];
                    
                    if(currentCat[count]){
                        console.log('si existe', count);
                        var y = currentCat[count];
                        var newValue = y[element2] + currentObject[element2];
                        y[element2] = newValue;
                        currentCat[count] = y;
                    } else {
                        console.log('no existe', count);
                        var obj = {};
                        obj[element2] = currentObject[element2]
                        currentCat[count] = obj;
                    }
                    count++;
                });
            });
        }
    }
    console.log(JSON.stringify(products));
    return products;
}

module.exports = router