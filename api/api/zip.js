var express = require('express');
var mysql = require('../utils/mysql');
var router = express.Router();

var cnx = mysql.connect();


router.get('/:code', function(req, res){
    getColoniasFromZip(req.params.code, function(error, result){
        if (error){
            res.statusCode = 400;
            res.send(error);
        } else {
            res.send(result)
        }
    })
});


async function getColoniasFromZip(code, callback){
    try {
        var res = await mysql.getColonias(code, cnx);
        if(res.length > 0){
            var tmp = {
                codigo_postal: code,
                municipio: res[0].municipio,
                estado: res[0].estado,
                colonias: []
            }

            Object.keys(res).forEach(index => {
                tmp.colonias.push(res[index].nombre)
            });
            callback('', tmp);
        } else {
            callback('', res);
        }
    } catch (e){
        callback(e, 'The query can not be executed');
    }
}

module.exports = router
