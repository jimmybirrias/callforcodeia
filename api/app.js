/*
Call For Code

Team name: Jalapeño Coders
Component: Cognos Embedded Dashboard
File description: Main file of analytics service 
*/
var config = require('config');
var express = require('express');
var bodyParser = require('body-parser');
var requireDir = require('require-dir');
var mysql = require('./utils/mysql');
var inventario = require('./utils/inventario');
var zip = require('./utils/zip');
var blockchain = require('./utils/blockchain');
var apis = requireDir('./api');
var path = './data/NewProductList.json';

var app = express();
var cnx = mysql.connect();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var port = process.env.PORT || 9999;

Object.keys(apis).forEach(function(api){
    if(api != 'products_weight'){
        console.log('Adding route ====> ./api/' + api);
        var tmp = require('./api/' + api);
        app.use('/api/' + api, tmp);
    }
});

app.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now(), 'Path', req.originalUrl);
    next();
});

app.get('/', function(req, res){
    res.json({
        node: process.version
    });
});

app.get('/test', function(req, res){
    mysql.queryColonia('LA CALMA', 'Zapopan', cnx, function(error, result){
        if(error){
            res.statusCode = 400;
            res.send(error);
        } else {
            res.json(result);
        }
    });
});

app.get('/inventario', function(req, res){
    inventario.read(path, function(error, result){
        if(error){
            res.statusCode = 400;
            res.send(error);
        } else {
            res.json(result);
        }
    });
});

app.get('/event', function(req, res){
    var id = 1;
    blockchain.getEvent(id, function(error, result){
        if(error){
            res.statusCode = 400;
            res.send(error);
        } else {
            res.json(result);
        }
    });
});

app.listen(port, function(){
    console.log('App is running in ====>', port);
});
